import MaterialTable from "material-table";
import styles from "./Tools.module.scss";
import React, { useState } from "react";
import { lookup, resolve } from "dns";
import { rejects } from "assert";
import Filters from "../../Filters/Filters";
import { useHistory } from "react-router";

const Tools = () => {
  let history = useHistory();
  const [tableData, setTableData] = useState([
    {
      name: "SD",
      operation: "T",
      make: "Spartan",
      type: "Square",
      subtype: "Square-Recess",
    },
    {
      name: "BS",
      operation: "C",
      make: "Stanley",
      type: "Tenon Saw",
      subtype: "Flat top",
    },
    {
      name: "T",
      operation: "TC",
      make: "Taparia",
      type: "MultiPurpose",
      subtype: "Electric",
    },
    {
      name: "CD",
      operation: "T",
      make: "Bosch",
      type: "Pistol drill type",
      subtype: "Step",
    },
    {
      name: "NP",
      operation: "G",
      make: "Knipex",
      type: "Long",
      subtype: "Steedy Steel",
    },
    {
      name: "Sp",
      operation: "T",
      make: "Stanley",
      type: "T Spanner",
      subtype: "LengthLT",
    },
    {
      name: "SD",
      operation: "T",
      make: "Taparia",
      type: "Slot Headed",
      subtype: "Hex Head",
    },
    {
      name: "BS",
      operation: "C",
      make: "Spartan",
      type: "Hand Saw",
      subtype: "Cross cut",
    },
    {
      name: "T",
      operation: "TC",
      make: "Spartan",
      type: "Line Tester",
      subtype: "Flat",
    },
    { name: "SD", operation: "T", make: "HTC", type: "Ubod", subtype: "Round" },
    {
      name: "H",
      operation: "FT",
      make: "Spartan",
      type: "Sledge Hammer",
      subtype: "Long handle",
    },
    {
      name: "Sp",
      operation: "T",
      make: "Knipex",
      type: "Socket Spanner",
      subtype: "Single end",
    },
    {
      name: "NP",
      operation: "G",
      make: "HTC",
      type: "Utility Pliers",
      subtype: "Single end",
    },
    {
      name: "D",
      operation: "T",
      make: "Bosch",
      type: "Hammer-Drill",
      subtype: "Rotary",
    },
    {
      name: "N",
      operation: "FX",
      make: "Stanley",
      type: "Round",
      subtype: "Long",
    },
    {
      name: "Tp",
      operation: "M",
      make: "HTC",
      type: "Diameter tape",
      subtype: "D-Type",
    },
  ]);

  const columns = [
    {
      title: "Tool Name",
      field: "name",
      lookup: {
        Tp: "Tape",
        N: "Nuts",
        Sp: "Spanner",
        H: "Hammer",
        D: "Drill",
        NP: "Nose Pliers",
        SD: "Screw-Driver",
        BS: "Back-Saw",
        T: "Tester",
        CD: "Cordless Drill",
      },
    },
    {
      title: "Operation",
      field: "operation",
      lookup: {
        T: "Tightner",
        C: "Cutter",
        TC: "Test Current",
        G: "Gripping",
        FT: "Fiter",
        M: "Measurement",
        FX: "Fixr",
      },
    },
    { title: "Make", field: "make" },
    { title: "Tool type", field: "type" },
    { title: "Tool Subtype", field: "subtype" },
    
  ];

  return (
    <div className={styles.page}>
      <div className={styles.filters}>
        <Filters/>
       
      </div>
      <div className={styles.table}>
        <MaterialTable
          columns={columns}
          data={tableData}
          title="Tools Data"
          editable={{
            onRowAdd:(newRow)=>new Promise((resolve, reject)=>{
              setTableData([...tableData, newRow])
              resolve(newRow)
            }),
            onRowUpdate:(newRow, oldRow)=>new Promise((resolve,reject)=>{
              setTimeout(()=>resolve(newRow),500)
            })
          }}
          actions={[
              {icon:()=><button
                onClick={() => {
                  history.push("/addtools");
                }}
                type="submit"
              >Edit</button>,
              tooltip:"Edit",
              onClick:(data)=>console.log(data)
            }
          ]}
          options={{
            showFirstLastPageButtons: false,
            sorting: true,
            search: true,
            filtering: true,
            addRowPosition:"first",
            actionsColumnIndex:-1,
          }}
        />
      </div>
    </div>
  );
};
export default Tools;
