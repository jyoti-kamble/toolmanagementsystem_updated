import React from "react";
import styles from "./AddTool.module.scss";
import { useForm } from "react-hook-form";
import { ClassificationTypeNames } from "typescript";
class AddTool extends React.Component {
  Opeartion = {
    operation: [
      "Tightner",
      "Cutter",
      "Fixer",
      "Fiter",
      "Test Current",
      "Gripping",
      "Measurement",
    ],
  };
  Make = { make: ["Stanley", "Taparia", "HTC", "Spartan", "Bosch", "Knipex"] };
  ToolType = {
    type: [
      "Square",
      "Tenon Saw",
      "MultiPurpose",
      "Pistol drill type",
      "Long",
      "T Spannenr",
      "Slot Headed",
      "Hand Saw",
      "Line Tester",
      "Ubod",
      "Sledge Hammer",
      "Socket Spanner",
      "Utility Pliers",
      "Hammer-Drill",
      "Round",
      "Diameter tape",
    ],
  };
  SubToolType = {
    subtype: [
      "D-Type",
      "Long",
      "Rotary",
      "Single end",
      "Long handle",
      "LengthLT",
      "Hex Head",
      "Cross cut",
      "Flat",
      "Round",
      "Square-Recess",
      "Flat top",
      "Electric",
      "Step",
      "Steedy Steel",
    ],
  };
  render() {
    const handleSubmit=(data: any)=>{
      console.log(data);
    }
    return (
      <div className={styles.filter}>
        <form onSubmit={handleSubmit}>
          <p>
            <label>Tool Name</label>
            <input name="tname" type="text" />
          </p>
          <p>
            <label>Operation</label>
            <select name="operation" className={styles.drpdwn}>
              {this.Opeartion.operation.map((data) => (
                <option title={data}> {data} </option>
              ))}
            </select>
          </p>
          <p>
            <label>Make</label>
            <select className={styles.drpdwn}>
              {this.Make.make.map((data) => (
                <option title={data}> {data} </option>
              ))}
            </select>
          </p>
          <p>
            <label>ToolType</label>
            <select className={styles.drpdwn}>
              {this.ToolType.type.map((data) => (
                <option title={data}> {data} </option>
              ))}
            </select>
          </p>
          <p>
            <label>Sub ToolType</label>
            <select className={styles.drpdwn}>
              {this.SubToolType.subtype.map((data) => (
                <option title={data}> {data} </option>
              ))}
            </select>
          </p>
          <p>
            <button className={styles.btn} >Add Tool</button>
          </p>
        </form>
      </div>
    );
  }
}

export default AddTool;
