import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import AddTool from "../AddTool/AddTool";
import Login from "../Login/Login";
import Home from "../Pages/Home";
import styles from './Main.module.scss';
const Main = () => {
  return (
    <div className={styles.main}>
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/addtools" component={AddTool} />
         
        </Switch>
      </Router>
    </div>
  );
};
export default Main;
