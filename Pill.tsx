import styles from './Pill.module.scss';
import { IProps } from './Pill.types';


const Pill = ({ text, onClick }: IProps) =><span 
    className={styles.nav}  onClick={onClick} > {text}  </span>

export default Pill;