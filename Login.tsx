import styles from "./Login.module.scss";
import { useForm } from "react-hook-form";
import Header from "../Header/Header";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Home from "../Pages/Home";

const schema = yup.object().shape({
  username: yup.string().required("Please enter username"),
  password: yup.string().required("Please enter password").min(5),
});

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  let history = useHistory();

  return (
    <div>
      <Header />
      <div className={styles.form}>
        <form >
          <div>
            <p>
              <label>Username</label>
              <input
                {...register("username")}
                type="text"
                className={styles.input}
              />
            </p>
          </div>
          <div>
            <p>
              <label>Password</label>
              <input
                type="password"
                {...register("password")}
                name="password"
                className={styles.input}
              />
            </p>
          </div>
          <p className={styles.messages}> {errors.message} </p>
          <div>
            <button
              onClick={() => {
                history.push("/home");
              }}
              type="submit"
              className={styles.btn}
            >
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default Login;
