import styles from "./Home.module.scss";
import Pill from "../Pill/Pill";
import { useState } from "react";
import Booking from "./Booking/Booking";
import Tools from "./Tools/Tools";
import Profile from "./Profile/Profile";
import { Route, Link } from "react-router-dom"

const Home = () => {
  const [page, setPage] = useState<string>("");

  const navigateTo = (url: string) => setPage(url);

  return (
    <div className={styles.main}>
      <div className={styles.panel}>
        <Pill text="Tools" onClick={() => navigateTo("tools")} />
       
        <Pill text="Booking" onClick={() => navigateTo("booking")} />
       
        <Pill text="My Profile" onClick={() => navigateTo("profile")} />
      </div>

      <div className={styles.info}>
        

        {page === "tools" && <Tools />}
        {page === "booking" && <Booking/> }
  {page === "profile" && <Profile/>}
      </div>
    </div>
  );
};
export default Home;
