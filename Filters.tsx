import React from "react";
import { useHistory } from "react-router";
import AddTool from "../AddTool/AddTool";
import styles from "./Filters.module.scss";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
class Filters extends React.Component{
     Opeartion={operation: ["Tightner", "Cutter", "Fixer","Fiter", "Test Current", "Gripping", "Measurement", ]}
     Make={make:["Stanley", "Taparia","HTC","Spartan", "Bosch","Knipex"]}
     ToolType={type:["Square", "Tenon Saw" , "MultiPurpose", "Pistol drill type" , "Long", "T Spannenr", "Slot Headed", "Hand Saw", "Line Tester","Ubod","Sledge Hammer","Socket Spanner","Utility Pliers","Hammer-Drill", "Round", "Diameter tape"]}
     SubToolType={subtype:["D-Type", "Long", "Rotary","Single end","Long handle", "LengthLT", "Hex Head", "Cross cut","Flat","Round","Square-Recess","Flat top","Electric","Step","Steedy Steel"]}
     //let history = useHistory();
     render()
   {
       return(
        <div className={styles.filter}>
           <select className={styles.drpdwn}>
               {this.Opeartion.operation.map(data=>(
                   <option title={data}> {data} </option>
               ))}
           </select>
           <select className={styles.drpdwn}>
               {this.Make.make.map(data=>(
                   <option title={data}> {data} </option>
               ))}
           </select>
           <select className={styles.drpdwn}>
               {this.ToolType.type.map(data=>(
                   <option title={data}> {data} </option>
               ))}
           </select>
           <select className={styles.drpdwn}>
               {this.SubToolType.subtype.map(data=>(
                   <option title={data}> {data} </option>
               ))}
           </select>
           <button className={styles.btn} 
           > Add Tool</button>
           
       </div>
       )
   } 
       
}

export default Filters;
